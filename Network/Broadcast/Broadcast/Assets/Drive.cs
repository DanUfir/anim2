﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.CrossPlatformInput;

public class Drive : MonoBehaviour {

	float speed = 10.0F;
	float rotationSpeed = 100.0F;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

		float trans = CrossPlatformInputManager.GetAxis ("Vertical") * speed;
		float rotation = CrossPlatformInputManager.GetAxis ("Horizontal") * rotationSpeed;

		trans *= Time.deltaTime;
		rotation *= Time.deltaTime;
		transform.Translate (0, 0, trans);
		transform.Rotate (0, rotation, 0);
	
	}
}
